int r = 20;

void setup() {
  size(500, 500);

  for (int i = 0; i < 10; i++) {
    if (isDivisible(i, 2)) {
      fill(255, 0, 0);
    } else {
      fill(0, 0, 255);
    }
    ellipse(position(i), 100, radius(r), radius(r));
    ellipse(position(i), 100, 20, 20);
    divide(20, 0);
  }
  int sum = addValues(10.5, 20.0);
  println(sum);
  
}

float radius(float radius) {
  return radius * 2;
}

int addValues(float a, float b) {
  return (int)(a + b);
}

boolean isDivisible(float a, float b) {
  return (a % b == 0);
}

float position(float a) {
  return a * 20 + 100;
}

void divide(int a, int b) {
  if (b == 0) {
    println("B was 0");
    return;
  }
  println("The result is: " + (a/b));
}