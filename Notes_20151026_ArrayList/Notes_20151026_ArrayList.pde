// Array of int, size 50
int[] array = new int[50];

// ArrayList of Integer, size increases as needed
ArrayList<Integer> arrayList = new ArrayList();

// Add 10 values to both
for (int i = 0; i < 10; i++) {
   array[i] = i; 
   arrayList.add(i);
}

// Print out all array values (many 0 values)
for (int i = 0; i < array.length; i++) {
   println(array[i]);
}

// Print out all arrayList values (only values added)
for (int i = 0; i < arrayList.size(); i++) {
   println(arrayList.get(i));
}

// Add one more value to the arrayList
arrayList.add(25);

// Iterate through arrayList without an index (ArrayList implements Iterable)
for (Integer i : arrayList) {
   println(i);
}