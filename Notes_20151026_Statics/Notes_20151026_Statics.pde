void setup() {
  Shape a = new Shape(10, 20);
  Shape b = new Shape(30, 40);
  
  // Print out values from Shape a and b
  println("A: " + a.getId() + " " + a.getX() + " " + a.getY());
  println("B: " + b.getId() + " " + b.getX() + " " + b.getY());
}

static class Shape {
  // static variable - one value per all Shape instances 
  private static int nextId;
  
  // instance variables - one value for each Shape instance
  private int id;
  private int x;
  private int y;

  Shape(int x, int y) {
    this.x = x;
    this.y = y;
    // Save id as next id and increment (note, not thread safe)
    id = nextId++;
  }

  int getId() {
    return id;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }
}