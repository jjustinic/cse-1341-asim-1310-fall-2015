size(500, 500);

// Declare some variables
int point1 = 600;
int point2 = 100;
int size1 = 200;

// Set background to white
background(255);

// Draw a green rectange using variables
fill(0, 255, 0);
rect(point1, point2, size1, size1);

// Set the stroke thickness of the shape
strokeWeight(10);

// Set the stroke color
stroke(10, 0, 255);

// Set the fill color
fill(255, 0, 0, 150);

// Draw a shape with custom points
beginShape();
vertex(50, 100);
vertex(point1, 200);
vertex(100, 300);
vertex(150, 200);
endShape(CLOSE);