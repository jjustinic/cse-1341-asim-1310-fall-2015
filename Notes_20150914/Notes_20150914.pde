int x = 0;

void setup() {
  size(500, 500);
  background(255);
  translate(width/2, height/2);
  for (int i = 1; i <= 5; i++) {
    pushMatrix();
    rotate(radians(i *360/5));
    drawMickey(0, 175);
    popMatrix();
  }
}

void drawMickey(int x, int y) {
  fill(0);
  ellipse(x, y, 100, 100);
  ellipse(x-50, y-50, 50, 50);
  ellipse(x+50, y-50, 50, 50);
}

void drawMickey(float x, float y) {
  fill(0);
  ellipse(x, y, 100, 100);
  ellipse(x-50, y-50, 50, 50);
  ellipse(x+50, y-50, 50, 50);
}

void drawMickeyTranslate(float x, float y) {
  fill(0);
  pushMatrix();
  translate(x, y);
  ellipse(0, 0, 100, 100);
  pushMatrix();
  translate(-50, -50);
  ellipse(0, 0, 50, 50);
  popMatrix();
  pushMatrix();
  translate(50, -50);
  ellipse(0, 0, 50, 50);
  popMatrix();
  popMatrix();
}