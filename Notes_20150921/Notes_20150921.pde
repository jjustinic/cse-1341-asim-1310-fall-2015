int[] x; // Declaring an integer array
x = new int[5]; // Allocate memory for 5 integers
x[0] = 1; // Initialize a value in the array by index. 
x[1] = 2;
x[2] = 3;
x[3] = 4;
x[4] = 5;

// Declare and allocate memory for an array in one line
int[] y = new int[5];
// Use a for loop to initialize all values in the array.
for (int i = 0; i < 5; i++) {
  y[i] = i + 1; // Initialize each value in the array
}

// Print the value of y at index 2.
println(y[2]);

// Array of 25 floats
float[] floatArray = new float[25];

// Array of 25 booleans
boolean[] booleanArray = new boolean[25];


int i = 0; // Declare i equal to 0

while (true) { // Loop indefinitely
  i++; // increment i
  if (i == 2) { 
    continue; // skip to the next iteration of the loop
  } else if (i == 5) {
    break; // exit out of the loop
  }
  println(i); // print the value of i
}