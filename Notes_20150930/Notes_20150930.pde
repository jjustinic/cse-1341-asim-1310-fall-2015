void setup() {
  size(500, 500);
  background(255);

  fill(0, 0, 255);
  triangle(100, 100, 200, 200, 200, 50);

  loadPixels();
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width/2; x++) {
      pixels[y*500+(width-1)-x] = pixels[y*500+x];
    }
  }
  updatePixels();
  
  loadPixels();
  for (int i = 0; i < pixels.length; i++) {
     pixels[i] = color(20, 30, 40);
     pixels[i] /= 4.4;
  }
  updatePixels();

  /*loadPixels();
   println(pixels.length);
   for (int i = 0; i < 20000; i++) {
   pixels[i] = color(255, 0, 0);
   }
   for (int i = 20000; i < 40000; i++) {
   pixels[i] = color(0, 0, 255); 
   }
   updatePixels();*/
}