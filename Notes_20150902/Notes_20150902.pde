size(200, 200);

background(255);

// Declare variable
int var;

// Assign the variable
var = 0;

// Use the variable
println(var);

// Declare variable and assign it a value
int integerVariable = 3;
float decimalVariable = 3.555;
boolean trueOrFalseVariable = false;
char characterVariable = '0';
String stringVariable = "Processing is fun";

// Sum integer and decimal. Cast float to int to prevent error.
int sum = integerVariable + (int) decimalVariable;

// Print out variables
println(sum);
println(trueOrFalseVariable);
println(characterVariable);
println(stringVariable);

// Declare size based on quarter of sketch width.
int size = width/4;

// Fill black or red depending on size of sketch.
if (size <= 100) {
  fill(0);
} else {
  fill(255, 0, 0); 
}

// Draw 4 ellipses using widths and heights.
ellipse(width/2 - size, height/2 - size, width/4, height/4);
ellipse(width/2 + size, height/2 - size, width/4, height/4);
ellipse(width/2 - size, height/2 + size, width/4, height/4);
ellipse(width/2 + size, height/2 + size, width/4, height/4);