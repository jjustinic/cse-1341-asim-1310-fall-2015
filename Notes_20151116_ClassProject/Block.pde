class Block extends GameElement {

  Sprite sprite = new Sprite("tiles-2.png");

  Block(float x, float y) {
    super(x, y, 32, 32); 
    loadSprites();
  }

  void loadSprites() {
    sprite.addSprite(0, 368, 0, 15, 15);
    sprite.addSprite(1, 368, 16, 15, 15);
    sprite.addSprite(2, 48, 0, 15, 15);
    sprite.resize((int) w, (int) h);
  }

  void collideAction(GameElement other) {
    sprite.setPosition(2);
  }

  void display() {
    sprite.display(x, y);
  }
}