class Mario extends GameElement {

  int movementSpeed = 7;

  int direction = 0;
  int position = 0;

  Sprite sprite = new Sprite("mario.png");

  public Mario(float x, float y, float w, float h, float gravity) {
    super(x, y, w, h, gravity);
    loadSprites();
  }

  void loadSprites() {
    // Add standing sprites
    sprite.addSprite(0, 211, 0, 14, 16);
    sprite.addSprite(1, 181, 0, 14, 16);

    // Add walking sprites
    for (int i = 0; i < 3; i++) {
      sprite.addSprite(2, 241 + (i * 30), 0, 14, 16);
      sprite.addSprite(3, 151 - (i * 30), 0, 14, 16);
    }

    // Add jumping sprites
    sprite.addSprite(4, 361, 0, 14, 16);
    sprite.addSprite(5, 31, 0, 14, 16);

    // Resize all sprites to desired size
    sprite.resize(28, 32);
  }

  void display() {
    int spritePosition = position * 2 + direction;
    sprite.setPosition(spritePosition);
    sprite.display(x, y);
  }

  void moveLeft() {
    xSpeed = movementSpeed;
    direction = 0;
    if (position != 2) {
      position = 1;
    }
  }

  void moveRight() {
    xSpeed = -movementSpeed;
    direction = 1;
    if (position != 2) {
      position = 1;
    }
  }

  void jump() {
    if (ySpeed == 0) {
      ySpeed = -movementSpeed;
      position = 2;
    }
  }

  void stand() {
    xSpeed = 0;
    if (ySpeed == 0) {
      position = 0;
    }
  }

  void setX(float x) {
    this.x = x;
  }
}