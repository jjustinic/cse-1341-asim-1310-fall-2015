class Mushroom extends GameElement {

  Sprite sprite = new Sprite("items.png");

  Mushroom(float x, float y) {
    super(x, y, 32, 32);
    loadSprites();
    xSpeed = 1;
  }

  void loadSprites() {
    sprite.addSprite(0, 186, 164, 16, 16);
    sprite.resize((int) w, (int) h);
  }

  void display() {
    sprite.display(x, y);
  }
}