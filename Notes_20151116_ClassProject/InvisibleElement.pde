class InvisibleElement extends GameElement {

  InvisibleElement(float x, float y, float w, float h) {
    super(x, y, w, h);
  }

  void display() {
    // Element is invisible, nothing do display
    // rect(x, y, w, h);
  }
}