abstract class GameElement {
  float x;
  float y;
  float w;
  float h;

  float xSpeed;
  float ySpeed;
  float gravity;

  GameElement(float x, float y, float w, float h) {
    this(x, y, w, h, 0);
  }

  GameElement(float x, float y, float w, float h, float gravity) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.gravity = gravity;
  }

  abstract void display();

  void move() {
    x += xSpeed;
    y += ySpeed;
    ySpeed += gravity;
  }

  boolean collide(GameElement other) {
    boolean collide = false;
    if (isBetween(getLeftEdge(), getRightEdge(), other.getLeftEdge(), other.getRightEdge())) {
      if (getBottomEdge() > other.getTopEdge() && getTopEdge() < other.getTopEdge()) {
        ySpeed = 0;
        other.collideAction(this);
        return true;
      }
      if (getTopEdge() < other.getBottomEdge() && getBottomEdge() > other.getBottomEdge()) {
        ySpeed *= -1;
        collide = true;
      }
    }  
    if (isBetween(getTopEdge(), getBottomEdge(), other.getTopEdge(), other.getBottomEdge())) {
      if (getRightEdge() > other.getLeftEdge() && getLeftEdge() < other.getLeftEdge()) {
        x = other.getLeftEdge() - w;
        xSpeed *= -1;
        collide = true;
      }
      if (getLeftEdge() < other.getRightEdge() && getRightEdge() > other.getRightEdge()) {
        x = other.getRightEdge();
        xSpeed *= -1;
        collide = true;
      }
    }
    if (collide) {
       other.collideAction(this); 
    }
    return collide;
  }

  void collideAction(GameElement other) {
  }

  float getLeftEdge() {
    return x;
  }

  float getTopEdge() {
    return y;
  }

  float getRightEdge() {
    return x + w;
  }

  float getBottomEdge() {
    return y + h;
  }

  boolean isBetween(float p1a, float p1b, float p2a, float p2b) {
    return (p1a > p2a && p1a < p2b) || (p1b > p2a && p1b < p2b);
  }
}