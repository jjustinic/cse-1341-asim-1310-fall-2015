class MushroomBlock extends Block {

  GameElement mushroom;

  MushroomBlock(float x, float y) {
    super(x, y);
  }

  void display() {
    super.display();
    if (mushroom != null) {
      mushroom.display();
    }
  }

  boolean collide(GameElement other) {
    boolean collide = super.collide(other);
    if (mushroom != null) {
      mushroom.collide(other);
    }
    return collide;
  }

  void collideAction(GameElement other) {
    println("New Mushroom");
    mushroom = new Mushroom(x - 32, y);
  }
}