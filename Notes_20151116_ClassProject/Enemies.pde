class Enemies extends GameElement {
  int imageWidth = 16;
  int imageHeight = 20;

  int spriteImgIndex = 0;

  int killFrame = -1;

  Sprite sprite = new Sprite("enemies.png");

  Enemies(float x, float y) {
    super(x, y, 32, 40);
    loadSprites();
    xSpeed = 1;
  }

  void loadSprites() {
    sprite.addSprite(0, 0, 0, 16, 20);
    sprite.addSprite(0, 30, 0, 16, 20);
    sprite.addSprite(1, 60, 0, 16, 16);
    sprite.addSprite(2, 0, 0, 1, 1);
    sprite.resize((int) w, (int) h);
  }

  void collideAction(GameElement other) {
    sprite.setPosition(1);
    killFrame = (killFrame == -1) ? frameCount : killFrame;
    h = 0;
  }

  void display() {
    if (sprite.getPosition() == 1 && frameCount - killFrame > 30) {
      sprite.setPosition(2);
    }

    sprite.display(x, y);
  }
}