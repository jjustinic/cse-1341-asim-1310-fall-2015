class Level {
  PImage levelImg = loadImage("1-1.png");
  float leftEdge = 0;
  float rightEdge = 6200;

  ArrayList<GameElement> levelElements = new ArrayList<GameElement>();

  Mario m = new Mario(100, height - 96, 28, 32, 0.2);

  public Level() {
    // Ground
    int groundLevel = 416;
    levelElements.add(new InvisibleElement(0, groundLevel, 6200, height - groundLevel));

    levelElements.add(new Block(512, 288));
    levelElements.add(new Block(672, 288));
    levelElements.add(new Block(704, 160));
    levelElements.add(new Block(736, 288));


    levelElements.add(new InvisibleElement(640, 288, 160, 32));
    levelElements.add(new InvisibleElement(896, 352, 64, 64));

    // Enemy
    levelElements.add(new Enemies(600, 376));
  }

  void display() {
    translate(-leftEdge, 0);
    image(levelImg, 0, 0, levelImg.width * 2, levelImg.height * 2);
    scrollLevel();

    if (keyPressed && keyCode == RIGHT) {
      m.moveLeft();
    } else if (keyPressed && keyCode == LEFT) {
      m.moveRight();
    } else {
      m.stand();
    }

    if (keyPressed && key == ' ') {
      m.jump();
    }

    m.move();

    for (GameElement levelElement : levelElements) {
      levelElement.move();
      m.collide(levelElement);
      for (GameElement otherElement : levelElements) {
        if (levelElement != otherElement) {
          levelElement.collide(otherElement);
        }
      }
      levelElement.display();
    }

    m.display();
  }

  void scrollLevel() {
    if (m.getLeftEdge() > leftEdge + 250) {
      if (leftEdge < rightEdge) {
        leftEdge += 7;
      }
    }

    if (m.getLeftEdge() <= leftEdge) {
      m.setX(leftEdge);
    }
  }
}