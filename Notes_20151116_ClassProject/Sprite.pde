class Sprite {

  PImage spriteSheet;
  ArrayList<ArrayList<PImage>> sprites;

  int position = 0;
  int frame = 0;

  Sprite(String spriteSheetName) {
    this.spriteSheet = loadImage(spriteSheetName);
    this.sprites = new ArrayList<ArrayList<PImage>>();
  }

  void addSprite(int position, int x, int y, int w, int h) {
    PImage sprite = createImage(w, h, ARGB);
    for (int r = 0; r < h; r++) {
      for (int c = 0; c < w; c++) {
        color clr = spriteSheet.get(x + c, y + r);
        sprite.set(c, r, clr);
      }
    }
    getPositionFrameList(position).add(sprite);
  }

  void display(float x, float y) {
    ArrayList<PImage> frames = getPositionFrameList(this.position);
    PImage currentFrame = frames.get(frame);
    image(currentFrame, x, y, currentFrame.width, currentFrame.height);
    if (frameCount % 5 == 0) {
      this.frame = (++frame < frames.size()) ? frame : 0;
    }
  }

  ArrayList<PImage> getPositionFrameList(int position) {
    while (position >= sprites.size()) {
      sprites.add(new ArrayList<PImage>());
    }
    return sprites.get(position);
  }

  void resize(int w, int h) {
    for (ArrayList<PImage> frames : sprites) {
      for (PImage frame : frames) {
        frame.resize(w, h);
      }
    }
  }

  int getPosition() {
    return position;
  }

  void setPosition(int position) {
    if (this.position != position) {
      this.frame = 0;
    }
    this.position = position;
  }
}