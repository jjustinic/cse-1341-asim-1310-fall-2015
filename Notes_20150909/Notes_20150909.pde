size(600, 600);
background(255);

pushMatrix();
rotate(radians(45));
translate(width/2, height/2);
ellipse(0, 0, 100, 500);

pushMatrix();
rotate(radians(90));
ellipse(0, 0, 100, 500);
popMatrix();
rotate(radians(45));
rect(100, 100, 100, 100);

//rotate(radians(180));

popMatrix();
rect(0, 0, 100, 100);