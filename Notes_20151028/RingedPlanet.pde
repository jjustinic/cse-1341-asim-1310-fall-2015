class RingedPlanet extends Planet {
  private color ringColor;
  private float ringSize;

  RingedPlanet(float distance, float speed, color clr, float size, color ringColor, float ringSize) {
    super(distance, speed, clr, size);
    this.ringColor = ringColor;
    this.ringSize = ringSize;
  }

  void displayBody() {
    super.displayBody();
    noFill();
    stroke(ringColor);
    strokeWeight(5);
    ellipse(0, 0, ringSize, ringSize/4);
  }
}