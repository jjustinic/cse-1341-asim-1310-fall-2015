class SolarSystem {

  Planet sun = new Planet(0, 0, color(255, 255, 0), 75);

  SolarSystem() {
    Planet mercury = new Planet(50, 2, color(175), 10);
    Planet venus = new Planet(70, 1.9, color(255, 145, 55), 18);
    Planet earth = new Planet(90, 1.75, color(0, 75, 255), 17);
    Planet mars = new Planet(110, 1.6, color(255, 0, 0), 12);
    Planet jupiter = new Planet(142, 1.4, color(255, 150, 55), 40);
    Planet saturn = new RingedPlanet(184, 1.1, color(200, 175, 0), 35, color(75,50, 0), 40);
    Planet uranus = new Planet(212, 1, color(55, 130, 255), 17);
    Planet neptune = new Planet(230, 0.9, color(10, 50, 255), 12);

    Planet earthMoon = new Planet(12, -5, color(225), 5);
    earth.addOrbiter(earthMoon);

    sun.addOrbiter(mercury);
    sun.addOrbiter(venus);
    sun.addOrbiter(earth);
    sun.addOrbiter(mars);
    sun.addOrbiter(jupiter);
    sun.addOrbiter(saturn);
    sun.addOrbiter(uranus );
    sun.addOrbiter(neptune);
  }

  void display() {
    sun.display();
  }
}