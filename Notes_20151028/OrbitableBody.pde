abstract class OrbitableBody {
  private ArrayList<OrbitableBody> orbitingBodies;

  private float distance;
  private float speed;

  private float position;

  OrbitableBody(float distance, float speed) {
    this.orbitingBodies = new ArrayList();
    this.distance = distance;
    this.speed = speed;
  }

  void addOrbiter(OrbitableBody orbitableBody) {
    orbitingBodies.add(orbitableBody);
  }

  void display() {
    pushMatrix();
    rotate(radians(position));
    translate(distance, 0);
    displayBody();
    for (OrbitableBody orbitableBody : orbitingBodies) {
      orbitableBody.display();
    }
    popMatrix();
    position += speed;
  }

  abstract void displayBody();
}