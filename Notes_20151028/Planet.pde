class Planet extends OrbitableBody {

  private color clr;
  private float size;

  Planet(float distance, float speed, color clr, float size) {
    super(distance, speed);
    this.clr = clr;
    this.size = size;
  }


  void displayBody() {
    fill(clr);
    stroke(0);
    strokeWeight(1);
    ellipse(0, 0, size, size);
  }
}