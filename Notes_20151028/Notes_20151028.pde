SolarSystem solarSystem = new SolarSystem();

void setup() {
   size(500, 500); 
}

void draw() {
   background(0);
   translate(width/2, height/2);
   solarSystem.display();
}