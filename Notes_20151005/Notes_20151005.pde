// Setup code, called once per sketch
void setup() {
  // Typical size and background method calls
  size(500, 500);
  background(255);

  // Declaring a int array of size 5
  int[] array = new int[5];
  // Setting a value at each valid index in the array
  array[0] = 0;
  array[1] = 0;
  array[2] = 0;
  array[3] = 0;
  array[4] = 0;

  int a = 1;
  int b = 2;

  // Using AND(&&), all must be true
  if (a < 2 && b < 22 && array[0] < 10) {
    println("Small numbers");
  }

  // Using OR(||), only 1 must be true
  if (a > 5 || b < 10) {
    println("second statement true");
  }

  // Using a for loop that counts by 5, from 50 to 100 inclusive
  for (int i = 50; i <= 100; i+=5) {
    if (i == 80) {
      continue; // skips to the next iteration if i is 80
    } else if (i== 90) {
      break; // exits the loop entirely if i is 90
    }
    println(i);
  }

  // Using a while loop that counts by 5, from 50 to 100 inclusive
  int k = 50;
  while (k <= 100) {
    println(k);
    k+=5;
  }

  // PIXEL ARRAYS

  loadPixels(); // must call loadPixels() before accessing pixel array
  for (int i = 0; i < pixels.length; i++) {
    // updating each pixel to be a random RGB value
    pixels[i] = color(random(150, 255), random(0, 100), random(0, 100));
  }
  updatePixels(); // must call updatePixels() after modifying pixel array

  // METHODS

  /* If given method call A or B, should be able to write out the
   corresponding method declaration. If given method declaration,
   should be able to write out the corrsponding method call.
   Most important part is correct return type and correct number
   and type of arguments.
   */

  // METHOD CALL A1
  int ans = divideNumbers(20.4, 10.2);
  println(ans);

  // METHOD CALL A2 - equivalent to A1, either is acceptable
  println(divideNumbers(20, 10));

  // METHOD CALL B
  boolean isOdd = isOdd(20);
  println(isOdd);
}

// METHOD DECLARATION A
int divideNumbers(float a, float b) {
  return (int) (a / b);
}

// METHOD DECLARATION B
boolean isOdd(int a) {
  // ! means not (inverse)
  return !(a % 2 == 0);
}