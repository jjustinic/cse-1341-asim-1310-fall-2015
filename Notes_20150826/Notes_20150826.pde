/* Example 1
   Wednesday, August 26
   Class Notes */

size(800, 800);

// Set background to white
background(255);

// Set fill color to red
fill(255, 0, 0);
rect(0, 0, 600, 200);
fill(0, 255, 0);
rect(50, 50, 400, 100);
fill(0, 0, 255);
ellipse(100, 100, 50, 100);
fill(255, 255, 0);
triangle(24, 75, 58, 0, 86, 75);