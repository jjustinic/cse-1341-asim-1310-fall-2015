size(600, 600);
background(255);

translate(width/2, height/2);

int shapeCount = 15;

for (int i = 0; i < shapeCount; i++) {
  pushMatrix();
  rotate(radians(i * (360/shapeCount)));
  
  pushMatrix();
  translate(0, 250);
  triangle(0, 25, -25, 0, 25, 0);
  rect(-5, 0, 10, -50);
  popMatrix();
  popMatrix();
}