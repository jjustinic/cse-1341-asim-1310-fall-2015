void setup() {
  size(500, 500);
  background(255);

  // Create a ball, call some methods and display
  Ball ball = new Ball(100);
  ball.setX(250);
  ball.setY(250);
  ball.setX(-10);
  ball.setY(399);
  ball.display();
  
  // Create a ball and display
  Ball ball2 = new Ball(50, 100, 100);
  ball2.display();
  
  // Create an array of type Ball (size 3)
  Ball[] balls = new Ball[3];
  
  // Loop through array, creating Ball instances and calling display
  for (int i = 0; i < balls.length; i++) {
    balls[i] = new Ball(25 + (25*i), 100 * i, 100 * i);
    balls[i].display();
  }
}

// Ball class - template to create Ball object instances.
class Ball {
  int radius;
  float x;
  float y;

  // Constructor - minimally require a radius
  Ball(int radius) {
    if (radius > 0) {
      this.radius = radius;
    } else {
      radius = 50;
    }
  }

  // Constructor - calls the other constructor before setting X and Y.
  Ball(int radius, float x, float y) {
    this(radius);
    setX(x);
    setY(y);
  }

  // Method to display this ball.
  void display() {
    ellipse(x, y, radius*2, radius*2);
  }

  // Setter to change the value of x
  void setX(float x) {
    if (x > radius && x < width - radius) {
      this.x = x;
    }
  }

  // Setter to change the value of y
  void setY(float y) {
    if (y > radius && y < height - radius) {
      this.y = y;
    }
  }
}