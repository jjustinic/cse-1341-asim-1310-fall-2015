// This class has a single String.
class SimpleString {
  private String string;

  // Require the String in the constructor
  SimpleString(String string) {
    this.string = string;
  }

  // Getter for the String instance variable
  String getString() {
    return string;
  }

  // Override equals() from java.lang.Object.
  // Always use equals for Object comparison, not ==.
  boolean equals(Object obj) {
    SimpleString other = (SimpleString) obj;
    return this.string.equals(other.getString());
  }

  // Override toString() from java.lang.Object.
  // Constrols how this object is printed out.
  public String toString() {
    return getClass() + " contains: " + getString();
  }

  // Override hashCode() from java.lang.Object.
  // Override hashCode() when overriding equals().
  public int hashCode() {
    return string.hashCode();
  }
}