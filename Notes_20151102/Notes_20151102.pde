
void setup() {
  SimpleString ss1 = new SimpleString("A");
  SimpleString ss2 = new SimpleString("A");
  SimpleString ss3 = ss1;
  println(ss1); // uses SimpleString toString()
  println(ss2); // uses SimpleString toString()
  println(ss3); // uses SimpleString toString()
  println(ss1 == ss2); // false but value is the same (A)
  println(ss1 == ss3); // true because references same instance
  println(ss1.equals(ss2)); // true

  String s1 = "A";
  String s2 = "A";
  // String has same result with equals or ==, but don't rely on this.
  // Internally, compiler optimizes to reference same String "A".
  println(s1 == s2); 
  println(s1.equals(s2));
}