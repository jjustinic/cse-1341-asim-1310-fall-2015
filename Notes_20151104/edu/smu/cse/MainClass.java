package edu.smu.cse;

public class MainClass {

    private String[] args;

    public static void main(String[] args) {
        MainClass mc = new MainClass(args);
        mc.printText();
    }
    
    public MainClass(String[] args) {
        this.args = args;
    }
    
    public void test() {}
    
    public void printText() {
        System.out.println("Hello World!");

        if (args.length > 0) {
            System.out.println(args[0]);
        }
        
        SecondClass sc = new SecondClass();
        sc.printText();
    }

}

