int ballCount = 100;

// Array of Ball object instances
Ball[] balls = new Ball[ballCount];

void setup() {
  size(500, 500);
  background(255);

  int startX = width / 5;
  int endX = startX * 4;
  int startY = height / 5;
  int endY = startY * 4;

  for (int i = 0; i < balls.length; i++) {
    float x = random(startX, endX);
    float y = random(startY, endY);
    
    // Instantiate each ball instance by calling constructor
    balls[i] = new Ball(x, y, random(5, 20));
    
    // Call setters for the color and speed
    balls[i].setColor(random(255), random(255), random(255));
    balls[i].setSpeed(random(1, 5), random(1, 5));
  }
}

void draw() {
  background(255);
  
  // Loop through all ball instances, calling move and display methods
  for (int i = 0; i < balls.length; i++) {
    balls[i].move();
    balls[i].display();
  }
}