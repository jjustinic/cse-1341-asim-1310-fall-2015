// Declare class Ball
class Ball {
  // Declare instance variables for each ball instance
  // Declaring variabes private is good practice,
  // but doesn't have an effect in Processing.
  private float x;
  private float y;
  private float xSpeed;
  private float ySpeed;
  private float radius;
  private color clr;

  // Constructor that requires x, y, and radius
  Ball(float x, float y, float radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;
  }

  // display the Ball with specified color
  void display() {
    fill(clr);
    ellipse(x, y, radius * 2, radius * 2);
  }

  // move the ball based on speed
  // alter the speed based on environment
  void move() {
    x += xSpeed;
    y += ySpeed;

    if (x < radius || x >= width - radius) {
      xSpeed *= -1;
    }

    if (y < radius || y >= height - radius) {
      ySpeed *= -1;
    }
  }

  // Setters for the color
  // setColor is "overloaded"

  // one argument setter for the color
  void setColor(float g) {
    this.clr = color(g);
  }

  // two argument setter for the color
  void setColor(float g, float a) {
    this.clr = color(g, a);
  }

  // three argument setter for the color
  void setColor(float r, float g, float b) {
    this.clr = color(r, g, b);
  }

  // four argument setter for the color
  void setColor(float r, float g, float b, float a) {
    this.clr = color(r, g, b, a);
  }

  // setter for the speed
  void setSpeed(float xSpeed, float ySpeed) {
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
  }
}