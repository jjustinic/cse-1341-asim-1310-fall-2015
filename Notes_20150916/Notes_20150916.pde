// Declare variables - cannot use values from void setup() {...}
int radius = 25;
float x;
float y;

// Called once per sketch
void setup() {
  size(500, 500);
  background(255);
  x = random(radius, width-radius);
}

// Called once per frame (~ 60 fps)
void draw() {
  background(255);
  for (int i = 0; i < 5; i++) {
    int diameter = radius * 2;
    drawBall(x + (i*diameter), y);
  }
  y++; // Increment y position by 1 each frame.
}

// Draw a red ball.
void drawBall(float x, float y) {
  fill(255, 0, 0);
  ellipse(x, y, radius*2, radius*2);
}