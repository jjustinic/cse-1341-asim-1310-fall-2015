class Circle extends Shape {

  Circle(float x, float y, float radius) {
    super(x, y, radius);
  }

  void displayShape() {
    ellipse(getX(), getY(), getSize(), getSize());
  }
}