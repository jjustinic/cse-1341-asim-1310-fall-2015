class Square extends Shape {

  Square(float x, float y, float sideLength) {
    super(x, y, sideLength);
  }

  void displayShape() {
    rectMode(CENTER);
    rect(getX(), getY(), getSize(), getSize());
  }
}