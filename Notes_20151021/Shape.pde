abstract class Shape {
  private float x;
  private float y;
  private float size;
  private float xSpeed;
  private float ySpeed;
  private color clr;

  Shape(float x, float y, float size) {
    this.x = x;
    this.y = y;
    this.size = size;
  }

  void move() {
    x += xSpeed;
    y += ySpeed;

    if (x < getHalfSize() || x > width - getHalfSize()) {
      xSpeed *= -1;
      x += xSpeed;
    }

    if (y < getHalfSize() || y > height - getHalfSize()) {
      ySpeed *= -1;
      y += ySpeed;
    }
  }

  float getX() {
    return x;
  }

  float getY() {
    return y;
  }

  float getSize() {
    return size;
  }

  float getHalfSize() {
    return size / 2;
  }

  void setColor(color clr) {
    this.clr = clr;
  }

  void setSpeed(float xSpeed, float ySpeed) {
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
  }

  final void display() {
    fill(clr);
    displayShape();
  }

  abstract void displayShape();
}

/*
Below interface and abstract class are exactly the same:
 
 interface class Shape {
 void display();
 void move();
 }
 
 abstract class Shape {
 abstract void display();
 abstract void move();
 }
 */