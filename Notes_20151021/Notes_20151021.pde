Shape[] shapes = new Shape[10];
int shapeSize = 30;

void setup() {
  size(500, 500);
  background(255);

  for (int i = 0; i < shapes.length; i++) {
    float x = random(shapeSize, width - shapeSize);
    float y = random(shapeSize, height - shapeSize);

    if (i % 2 == 0) {
      shapes[i] = new Circle(x, y, shapeSize);
    } else {
      shapes[i] = new Square(x, y, shapeSize);
    }

    shapes[i].setColor(color(random(255), random(255), random(255)));
    shapes[i].setSpeed(random(-3, 3), random(-3, 3));
  }
}

void draw() {
  background(255);

  for (int i = 0; i < shapes.length; i++) {
    shapes[i].move();
    shapes[i].display();
  }
}