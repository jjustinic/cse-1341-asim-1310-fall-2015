size(500, 500); 
background(255);

// Transformation example
// ANSWER (TOP LEFT TO BOTTOM RIGHT): CBADE
rectMode(CENTER);
pushMatrix();
ellipse(250, 250, 50, 50); // A
translate(100, 100); 
ellipse(50, 50, 50, 50);  // B
pushMatrix(); 
translate(150, 150); 
ellipse(-200, -200, 50, 50); // C
popMatrix();
ellipse(250, 250, 50, 50); // D
popMatrix();
ellipse(450, 450, 50, 50); // E


// IF / ELSE EXAMPLE
int num = 12;

if (num > 10) { 
  println("LG");
} else { 
  println("SM");
}

// IF / ELSE IF / ELSE EXAMPLE
int num2 = 0;

if (num2 < 0) { 
  println("1");
} else if (num2 > 0) { 
  println("2");
} else {
  println("else"); 
}

// INCREMENTATION EXAMPLE
int num3 = 20;
num3 += 500; // same as: num = num + 500;
println(num3);

// All three do the same thing:
// i++;
// i += 1;
// i = i + 1;

// Runs 10 times, incrementing num3 by 1.
for (int i = 1; i <= 30; i+=3) { 
  num3++;
} 

println(num3);

// INCREMENTATION EXAMPLE #2
int i = 20;

println(i++);

int c = i++;

println(++i);

println(c);

// VARIABLE TYPES AND CASTING EXAMPLE

float num4 = 21;

float ans = num4 / 2;
int rem = (int) num4 % 2;
println("ANS: " + ans + " REM: " + rem);

float a = 3.5;
float b = 2.7;
int intSum = (int) (a + b);
float sum = a + b;
println(intSum);
println(sum);