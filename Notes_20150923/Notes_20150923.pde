int numberOfBalls = 10;
float[] x = new float[numberOfBalls];
float[] y = new float[numberOfBalls];
float paddle1X;
float paddle2X;
float paddle1Y;
float paddle2Y;
float[] xSpeed = new float[numberOfBalls];
float[] ySpeed = new float[numberOfBalls];
int paddleDistance = 50;

void setup() {
  size(500, 500);
  rectMode(CENTER);
  for (int i = 0; i < numberOfBalls; i++) {
    x[i] = width/2 + (10 * i);
    y[i] = height/2;
    xSpeed[i] = random(0.5, 1.5);
    ySpeed[i] = random(0.1, 0.3);
  }
  paddle1X = paddleDistance;
  paddle1Y = height/2;
  paddle2X = width - paddleDistance;
  paddle2Y = height/2;
}

void draw() {
  background(0);
  fill(255);
  drawBalls();
  drawPaddle1();
  drawPaddle2();
  move();
  checkForWinner();
}

void keyPressed() {
  if (key == 'w') {
    paddle1Y -= 5;
  } else if (key == 's') {
    paddle1Y += 5;
  } else if (keyCode == UP) {
    paddle2Y += 5;
  } else if (keyCode == DOWN) {
    paddle2Y += 5;
  }
}

void drawBalls() {
  for (int i = 0; i < x.length; i++) {
    ellipse(x[i], y[i], 20, 20);
  }
}

void drawPaddle1() {
  rect(paddle1X, paddle1Y, 25, 100);
}

void drawPaddle2() {
  rect(paddle2X, paddle2Y, 25, 100);
}

void move() {
  // move ball
  for (int i = 0; i < x.length; i++) {
    x[i] += xSpeed[i]; 
    y[i] += ySpeed[i];

    // check if ball hit left paddle
    float paddle1Right = paddle1X + 12.5;
    float paddle1Top = paddle1Y - 50;
    float paddle1Bottom = paddle1Y + 50;

    if (x[i] - 10 < paddle1Right && y[i] > paddle1Top && y[i] < paddle1Bottom) {
      xSpeed[i] *= -1;
    }

    // check if ball hit right paddle
    float paddle2Left = paddle2X - 12.5;
    float paddle2Top = paddle2Y - 50;
    float paddle2Bottom = paddle2Y + 50;

    if (x[i] + 10 > paddle2Left && y[i] > paddle2Top && y[i] < paddle2Bottom) {
      xSpeed[i] *= -1;
    }

    // check if paddle hit top/bottom of screen
    if (y[0] -10 < 0 || y[0] + 10 > height) {
      ySpeed[i] *= -1;
    }
  }
}

void checkForWinner() {
  for (int i = 0; i < x.length; i++) {
    // check if paddle hit left or right edge of screen
    if (x[i] < 0) {
      println("The winner is player 2");
    } else if (x[i] > width) {
      println("The winner is player 1");
    }
  }
}