int[][] board = new int[6][7];
int player = 1;
int winner = 0;

void setup() {
  size(700, 700);
  background(255);
}

void draw() {
  background(255);
  drawBoard();
  if (winner == 0) {
    drawNextPiece();
  } else {
    println("The winner is player " + winner);
  }
}

void mousePressed() {
  if (winner == 0) {
    makeMove(mouseX);
  }
}

void drawBoard() {
  noStroke();
  fill(255, 255, 0);
  rect(0, 100, 700, 600);
  ellipseMode(CORNER);
  stroke(0);
  for (int r = 0; r < 6; r++) {
    for (int c = 0; c < 7; c++) {
      if (board[r][c] == 1) {
        fill(255, 0, 0);
      } else if (board[r][c] == 2) {
        fill(0);
      } else {
        fill(255);
      }
      ellipse(c * 100, 100 + (r * 100), 100, 100);
    }
  }
}

void drawNextPiece() {
  if (player == 1) {
    fill(255, 0, 0);
  } else if (player == 2) {
    fill(0);
  }
  int column = calculateColumn(mouseX);
  ellipse(column * 100, 0, 100, 100);
}

void makeMove(int x) {
  int c = calculateColumn(x);
  for (int r = 5; r >= 0; r--) {
    if (board[r][c] == 0) {
      board[r][c] = player;
      checkHorizontalWinner();
      checkVerticalWinner();
      checkDiagonalWinner();
      if (player == 1) { 
        player = 2;
      } else {
        player = 1;
      }
      break;
    }
  }
}

int calculateColumn(int x) {
  int column = x / 100;
  return column;
}

void checkHorizontalWinner() {
  for (int r = 0; r < 6; r++) {
    for (int c = 0; c <= 3; c++) {
      if (board[r][c] != 0 && board[r][c] == board[r][c+1] && board[r][c] == board[r][c+2] && board[r][c] == board[r][c+3]) {
        winner = player;
      }
    }
  }
}

void checkVerticalWinner() {
  for (int r = 0; r <= 2; r++) {
    for (int c = 0; c < 7; c++) {
      if (board[r][c] != 0 && board[r][c] == board[r+1][c] && board[r][c] == board[r+2][c] && board[r][c] == board[r+3][c]) {
        winner = player;
      }
    }
  }
}

void checkDiagonalWinner() {
  for (int r = 0; r < 3; r++) {
    for (int c = 0; c < 4; c++) {
      if (board[r][c] != 0 && board[r][c] == board[r+1][c+1] && board[r][c] == board[r+2][c+2] && board[r][c] == board[r+3][c+3]) {
        winner = player;
      }
    }
  }

  for (int r = 0; r < 3; r++) {
    for (int c = 3; c < 7; c++) {
      if (board[r][c] != 0 && board[r][c] == board[r+1][c-1] && board[r][c] == board[r+2][c-2] && board[r][c] == board[r+3][c-3]) {
        winner = player;
      }
    }
  }
}